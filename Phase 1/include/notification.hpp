#ifndef __NOTIFICATION_H__
#define __NOTIFICATION_H__

#include <iostream>
#include <string>

class Notification
{
public:
	Notification(std::string _username, std::string _id);
	virtual void print() = 0;
protected:
	std::string username, id;
};

class LikeNotification : public Notification
{
public:
	LikeNotification(std::string _username, std::string _id): Notification(_username, _id) {};
	void print();
};

class DisLikeNotification : public Notification
{
public:
	DisLikeNotification(std::string _username, std::string _id): Notification(_username, _id) {};
	void print();
};

class CommentNotification : public Notification
{
public:
	CommentNotification(std::string _username, std::string _id): Notification(_username, _id) {};
	void print();
};

class ReplyNotification : public Notification
{
public:
	ReplyNotification(std::string _username, std::string _id): Notification(_username, _id) {};
	void print();
};

class JeekNotification : public Notification
{
public:
	JeekNotification(std::string _username, std::string _id): Notification(_username, _id) {};
	void print();
};

class MentionNotification : public Notification
{
public:
	MentionNotification(std::string _username, std::string _id): Notification(_username, _id) {};
	void print();
};

class RejeekNotification : public Notification
{
public:
	RejeekNotification(std::string _username, std::string _id): Notification(_username, _id) {};
	void print();
};

#endif