#ifndef __UTILITY_H__
#define __UTILITY_H__

#include <string>

std::string removePrecedingSpaces(const std::string& str);
std::string intToStr(int n);
int strToInt(std::string s);

#endif