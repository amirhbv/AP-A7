#ifndef __JEEK_H__
#define __JEEK_H__

#include <string>
#include <vector>
#include <iostream>

class Jeek
{
public:
	Jeek(std::string _userId, std::string _jeekText, std::vector<std::string> _tags, std::vector<std::string> _mentions);
	void print();
	void searchPrint();
	std::string getId() {return id; };
	std::string getUserId() {return userId; };
	void setId(std::string _id) { id = _id; };
	void addComment(std::string commentId);
	void reJeek();
	void like(std::string userId);
	void dislike(std::string userId);
private:
	std::string id;
	std::string userId;
	std::string jeekText;
	std::vector<std::string> tags;
	std::vector<std::string> mentions;
	std::vector<std::string> likers;
	std::vector<std::string> commentIds;
	int reJeekCount;
};

#endif