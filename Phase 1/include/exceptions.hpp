#ifndef EXCEPTIONS_H
#define EXCEPTIONS_H

#include <string>


class Exception
{
public:
	virtual std::string what() {return "";};
};

class InvalidCommand: public Exception
{
public:
	std::string what()
	{
		return "This command is not available!";
	}
};

class DuplicateUsername: public Exception
{
public:
	std::string what()
	{
		return "This username is already taken!";
	}
};

class LoginException: public Exception
{
public:
	std::string what()
	{
		return "You must login to jeek!";
	}
};

class JeekException: public Exception
{
public:
	std::string what()
	{
		return "Jeek text must be less that 140 character!";
	}
};

class UserNotAvailable: public Exception
{
public:
	std::string what()
	{
		return "username doesn't exists!";
	}
};

class JeekNotAvailable: public Exception
{
public:
	std::string what()
	{
		return "jeek id doesn't exists!";
	}
};

class IdNotAvailable: public Exception
{
public:
	std::string what()
	{
		return "comment or reply id doesn't exists!";
	}
};

class CommentNotAvailable: public Exception
{
public:
	std::string what()
	{
		return "comment id doesn't exists!";
	}
};

class ReplyNotAvailable: public Exception
{
public:
	std::string what()
	{
		return "reply id doesn't exists!";
	}
};
#endif
