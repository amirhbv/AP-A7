#ifndef __INPUT_MANAGER_H__
#define __INPUT_MANAGER_H__

#include <string>

enum CommandName
{
	SIGNUP = 0,
	LOGIN,
	LOGOUT,
	NEW_JEEK,
	JEEK_TEXT,
	JEEK_TAG,
	JEEK_MENTION,
	JEEK_PUBLISH,
	JEEK_ABORT,
	SHOW_JEEK,
	SERACH,
	NEW_COMMENT,
	SHOW_COMMNET,
	NEW_REPLY,
	SHOW_REPLY,
	REJEEK,
	JEEK_LIKE,
	JEEK_DISLIKE,
	FOLLOW,
	UNFOLLOW,
	SHOW_NOTIFICATIONS

};
const std::string commandNames[]
{
	"signup",
	"login",
	"logout",
	"jeek",
	"text",
	"tag",
	"mention",
	"publish",
	"abort",
	"showJeek",
	"search",
	"comment",
	"showComment",
	"reply",
	"showReply",
	"rejeek",
	"like",
	"dislike",
	"follow",
	"unfollow",
	"notifications"
};

class InputManager
{
public:
	InputManager() {};
	void run();
private:
	std::string command;
	void handleCommand();

	void signUp();
	void logIn();
	void logOut();
	void newJeek();
	void search();
	void comment();
	void reply();
	void reJeek();
	void showJeek();
	void showComment();
	void showReply();
	void like();
	void dislike();
	void follow();
	void unfollow();
	void notifications();
};

#endif