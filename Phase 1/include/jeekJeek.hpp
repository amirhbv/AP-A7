#ifndef __JEEK_JEEK_H__
#define __JEEK_JEEK_H__

#include <string>
#include <vector>

#include "user.hpp"
#include "jeek.hpp"
#include "reply.hpp"
#include "logger.hpp"
#include "comment.hpp"
#include "exceptions.hpp"
#include "dataManager.hpp"
#include "inputManager.hpp"
#include "notification.hpp"

const int MAXIMUM_JEEK_LENGTH = 140;

class JeekJeek
{
public:
	JeekJeek();

	static JeekJeek* instance();
	DataManager* data() { return dataManager; }

	void run();

	bool isUserActive();
	bool isJeekTextOk(std::string jeekText);

	void addUser(std::string username, std::string displayName,  std::string password);
	void loginUser(std::string username, std::string password);
	void logOut();
	void addJeek(std::string jeekText, std::vector<std::string> tags, std::vector<std::string> mentions);
	void searchUser(std::string query);
	void searchTag(std::string query);
	void addComment(std::string jeekId, std::string commentText);
	void addReply(std::string id, std::string replyText);
	void replyComment(std::string id, std::string replyText);
	void replyReply(std::string id, std::string replyText);
	void reJeek(std::string jeekId);
	void showJeek(std::string jeekId);
	void showComment(std::string commentId);
	void showReply(std::string replyId);
	void like(std::string jeekId);
	void dislike(std::string jeekId);
	void follow(std::string userId);
	void unfollow(std::string userId);
	void showNotifications();

private:
	static JeekJeek* _instance;

	InputManager *inputManager;
	DataManager *dataManager;
	std::string activeUserId;

};

#endif