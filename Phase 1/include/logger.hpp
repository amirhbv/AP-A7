#ifndef __LOGGER_H__
#define __LOGGER_H__

#include <fstream>

class Logger
{
public:
	Logger(std::string fileName);
	~Logger();

	void log(std::string content);

	static Logger* instance();

private:
	static Logger* _instance;

	std::ofstream fout;
};

#endif