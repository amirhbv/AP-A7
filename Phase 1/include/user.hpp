#ifndef __USER_H__
#define __USER_H__

#include <string>
#include <vector>

#include "notification.hpp"

class User
{
public:
	User(std::string username, std::string _displayName,  std::string _password);

	std::string getId() {return id; };
	std::string getName() {return displayName; };
	std::vector<std::string> getFollowers() {return followers;};
	bool checkPassword(std::string _password) { return password == _password; };
	void addJeek(std::string jeekId);
	void printJeeks();
	void addFollower(std::string userId);
	void removeFollower(std::string userId);
	void addNotification(Notification* notification);
	void showNotifications();
private:
	std::string id;
	std::string password;
	std::string displayName;
	std::vector<std::string> jeekIds;
	std::vector<std::string> followers;
	std::vector<Notification*> notifications;
};

#endif