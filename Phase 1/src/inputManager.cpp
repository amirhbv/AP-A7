#include "inputManager.hpp"

#include "jeekJeek.hpp"
#include "utility.hpp"
#include <iostream>

void InputManager::run()
{
	Logger logger("error.txt");
	while (std::cin >> command)
	{
		logger.log("command : " + command);
		//std::cout << std::endl;
		try {
			handleCommand();
		}
		catch (Exception &e)
		{
			//std::cerr << e.what() << std::endl;
			logger.log(e.what());
		}
		catch (...)
		{

		}
	}
}

void InputManager::handleCommand()
{
	if (command.compare("signup") == 0 )
	{
		signUp();
	}
	else if (command.compare("login") == 0 )
	{
		logIn();
	}
	else if (command.compare("logout") == 0 )
	{
		logOut();
	}
	else if (command.compare("jeek") == 0 )
	{
		newJeek();
	}
	else if (command.compare("search") == 0 )
	{
		search();
	}
	else if (command.compare("comment") == 0 )
	{
		comment();
	}
	else if (command.compare("reply") == 0 )
	{
		reply();
	}
	else if (command.compare("reJeek") == 0 )
	{
		reJeek();
	}
	else if (command.compare("showJeek") == 0 )
	{
		showJeek();
	}
	else if (command.compare("showComment") == 0 )
	{
		showComment();
	}
	else if (command.compare("showReply") == 0 )
	{
		showReply();
	}
	else if (command.compare("like") == 0 )
	{
		like();
	}
	else if (command.compare("dislike") == 0 )
	{
		dislike();
	}
	else if (command.compare("follow") == 0 )
	{
		follow();
	}
	else if (command.compare("unfollow") == 0 )
	{
		unfollow();
	}
	else if (command.compare("notifications") == 0 )
	{
		notifications();
	}
	else
	{
		throw InvalidCommand();
	}
}

void InputManager::signUp()
{
	std::string username, password, displayName;
	std::cin >> username >> displayName >> password ;
	JeekJeek::instance()->addUser(username, displayName, password);
}

void InputManager::logIn()
{
	std::string username, password;
	std::cin >> username >> password;
	JeekJeek::instance()->loginUser(username, password);
}

void InputManager::logOut()
{
	JeekJeek::instance()->logOut();
}

void InputManager::newJeek()
{
	std::string temp;
	std::string text;
	std::vector<std::string> tags;
	std::vector<std::string> mentions;
	std::cin >> command;
	while (command.compare("publish") != 0 && command.compare("abort") != 0)
	{
		getline(std::cin, temp);
		temp = removePrecedingSpaces(temp);
		if (command.compare("text") == 0)
		{
			text = temp;
		}
		else if (command.compare("tag") == 0)
		{
			tags.push_back(temp);
		}
		else if (command.compare("mention") == 0)
		{
			mentions.push_back(temp);
		}
		std::cin >> command;
	}
	if (command == "publish")
	{
		JeekJeek::instance()->addJeek(text, tags, mentions);
	}

}

void InputManager::search()
{
	std::string content;
	std::cin >> content;
	if (content[0] == '@')
	{
		JeekJeek::instance()->searchUser(content.substr(1));
	}
	else if (content[0] == '#')
	{
		JeekJeek::instance()->searchTag(content.substr(1));
	}
}

void InputManager::comment()
{
	std::string id, text;
	std::cin >> id;
	getline(std::cin, text);
	text = removePrecedingSpaces(text);
	JeekJeek::instance()->addComment(id, text);
}

void InputManager::reply()
{
	std::string id, text;
	std::cin >> id;
	getline(std::cin, text);
	text = removePrecedingSpaces(text);
	JeekJeek::instance()->addReply(id, text);
}

void InputManager::reJeek()
{
	std::string id, text;
	std::cin >> id;
	JeekJeek::instance()->reJeek(id);
}

void InputManager::showJeek()
{
	std::string id;
	std::cin >> id;
	JeekJeek::instance()->showJeek(id);
}

void InputManager::showComment()
{
	std::string id;
	std::cin >> id;
	JeekJeek::instance()->showComment(id);
}

void InputManager::showReply()
{
	std::string id;
	std::cin >> id;
	JeekJeek::instance()->showReply(id);
}

void InputManager::like()
{
	std::string id;
	std::cin >> id;
	JeekJeek::instance()->like(id);
}

void InputManager::dislike()
{
	std::string id;
	std::cin >> id;
	JeekJeek::instance()->dislike(id);
}

void InputManager::follow()
{
	std::string userId;
	std::cin >> userId;
	JeekJeek::instance()->follow(userId);
}

void InputManager::unfollow()
{
	std::string userId;
	std::cin >> userId;
	JeekJeek::instance()->unfollow(userId);
}

void InputManager::notifications()
{
	JeekJeek::instance()->showNotifications();
}