#include "logger.hpp"

Logger* Logger::_instance = nullptr;

Logger* Logger::instance()
{
	if (Logger::_instance == nullptr)
	{
		Logger::_instance = new Logger("log.txt");
	}
	return Logger::_instance;
}

Logger::Logger(std::string fileName)
{
	fout.open(fileName, std::ios_base::app);

	if (fout.fail())
	{
		throw std::iostream::failure("Cannot open file: " + fileName);
	}

	fout << std::endl << __DATE__ << ' ' << __TIME__ << std::endl;
}

void Logger::log(std::string content)
{
	fout << content << std::endl;
}

Logger::~Logger()
{
	if (fout.is_open())
	{
		fout.close();
	}
}