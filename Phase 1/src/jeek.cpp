#include "jeek.hpp"

#include <algorithm>

#include "jeekJeek.hpp"

Jeek::Jeek(std::string _userId, std::string _jeekText, std::vector<std::string> _tags, std::vector<std::string> _mentions)
{
	userId = _userId;
	jeekText = _jeekText;
	tags = _tags;
	mentions = _mentions;
	reJeekCount = 0;
}

void Jeek::print()
{
	std::cout << JeekJeek::instance()->data()->findUserById(userId)->getName() << std::endl;
	std::cout << jeekText << std::endl;
	for (auto tag : tags)
		std::cout << "#" << tag << std::endl;
	for (auto userId : mentions)
		std::cout << "@" << userId << std::endl;
	std::cout << "Likes " << likers.size() << std::endl;
	std::cout << "Rejeeks " << reJeekCount << std::endl;
	std::cout << "comments:" << std::endl;
	for (auto commentId : commentIds)
		std::cout << commentId << std::endl;
}

void Jeek::searchPrint()
{
	std::cout << id << ' ' << JeekJeek::instance()->data()->findUserById(userId)->getName() << std::endl;
	std::cout << jeekText << std::endl;
}

void Jeek::addComment(std::string commentId)
{
	commentIds.push_back(commentId);
}

void Jeek::reJeek()
{
	JeekJeek::instance()->addJeek("Rejeeked:" + jeekText, tags, mentions);
	reJeekCount++;
}

void Jeek::like(std::string userId)
{
	std::vector<std::string>::iterator it;
	it = find (likers.begin(), likers.end(), userId);
	if (it == likers.end())
	{
		likers.push_back(userId);
	}
}

void Jeek::dislike(std::string userId)
{
	std::vector<std::string>::iterator it;
	it = find (likers.begin(), likers.end(), userId);
	if (it != likers.end())
	{
		likers.erase(it);
	}
}