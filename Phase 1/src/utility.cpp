#include "utility.hpp"

#include <sstream>

std::string removePrecedingSpaces(const std::string& str)
{
	int i = 0;
	while (str[i] == ' ')
		i++;
	return str.substr(i);
}

std::string intToStr(int n)
{
	std::stringstream s;
	s << n;
	return s.str();
}

int strToInt(std::string s)
{
	std::stringstream ss(s);
	int res;
	ss >> res;
	return res;
}
