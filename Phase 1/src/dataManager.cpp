#include "dataManager.hpp"

User* DataManager::findUserById(std::string id)
{
	User* user = NOT_FOUND;
	for (unsigned int i = 0; i < users.size(); i++)
	{
		if (users[i]->getId() == id)
		{
			user = users[i];
		}
	}
	return user;
}

Jeek* DataManager::findJeekById(std::string id)
{
	Jeek* jeek = NOT_FOUND;
	for (unsigned int i = 0; i < jeeks.size(); i++)
	{
		if (jeeks[i]->getId() == id)
		{
			jeek = jeeks[i];
		}
	}
	return jeek;
}

Comment* DataManager::findCommentById(std::string id)
{
	Comment* comment = NOT_FOUND;
	for (unsigned int i = 0; i < comments.size(); i++)
	{
		if (comments[i]->getId() == id)
		{
			comment = comments[i];
		}
	}
	return comment;
}

Reply* DataManager::findReplyById(std::string id)
{
	Reply* reply = NOT_FOUND;
	for (unsigned int i = 0; i < replies.size(); i++)
	{
		if (replies[i]->getId() == id)
		{
			reply = replies[i];
		}
	}
	return reply;
}

std::string DataManager::makeJeekId()
{
	std::string id;
	if (!jeeks.size())
		id = intToStr(0);
	else
		id = intToStr(strToInt(jeeks.back()->getId().substr(1)) + 1);
	return 'J' + id;
}
std::string DataManager::makeCommentId()
{
	std::string id;
	if (!comments.size())
		id = intToStr(0);
	else
		id = intToStr(strToInt(comments.back()->getId().substr(1)) + 1);
	return 'C' + id;
}
std::string DataManager::makeReplyId()
{
	std::string id;
	if (!replies.size())
		id = intToStr(0);
	else
		id = intToStr(strToInt(replies.back()->getId().substr(1)) + 1);
	return 'R' + id;
}

void DataManager::addUser(User* user)
{
	users.push_back(user);
}

void DataManager::addJeek(Jeek* jeek)
{
	jeek->setId(makeJeekId());
	jeeks.push_back(jeek);
}

void DataManager::addComment(Comment* comment)
{
	comment->setId(makeCommentId());
	comments.push_back(comment);
}

void DataManager::addReply(Reply* reply)
{
	reply->setId(makeReplyId());
	replies.push_back(reply);
}

void DataManager::addTag(std::string tag, std::string jeekId)
{
	std::vector<std::string>::iterator it;
	it = find (tagMap[tag].begin(), tagMap[tag].end(), jeekId);
	if (it == tagMap[tag].end())
	{
		tagMap[tag].push_back(jeekId);
	}
}

std::vector<std::string> DataManager::findTag(std::string tag)
{
	return tagMap[tag];
}