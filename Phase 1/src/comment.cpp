#include "comment.hpp"

#include <iostream>

#include "jeekJeek.hpp"

Comment::Comment(std::string _userId, std::string text)
{
	userId = _userId;
	commentText = text;
}

void Comment::addReply(std::string replyId)
{
	replies.push_back(replyId);
}

void Comment::print()
{
	std::cout << JeekJeek::instance()->data()->findUserById(userId)->getName() << std::endl;
	std::cout << commentText << std::endl;
	std::cout << "Replies:" << std::endl;
	for (auto replyId : replies)
		std::cout << replyId << std::endl;
}
