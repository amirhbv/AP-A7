#include "jeek.hpp"

#include <algorithm>

#include "jeekJeek.hpp"
#include "../utils/utilities.hpp"
#include "utility.hpp"

Jeek::Jeek(std::string _userId, std::string _jeekText, std::vector<std::string> _tags, std::vector<std::string> _mentions)
{
	writerId = _userId;
	jeekText = _jeekText;
	tags = _tags;
	mentions = _mentions;
	reJeekCount = 0;
}

void Jeek::print()
{
	std::cout << JeekJeek::instance()->data()->findUserById(writerId)->getName() << std::endl;
	std::cout << jeekText << std::endl;
	for (auto tag : tags)
		std::cout << "#" << tag << std::endl;
	for (auto userId : mentions)
		std::cout << "@" << userId << std::endl;
	std::cout << "Likes " << likers.size() << std::endl;
	std::cout << "Rejeeks " << reJeekCount << std::endl;
	std::cout << "comments:" << std::endl;
	for (auto commentId : commentIds)
		std::cout << commentId << std::endl;
}

void Jeek::searchPrint()
{
	std::cout << id << ' ' << JeekJeek::instance()->data()->findUserById(writerId)->getName() << std::endl;
	std::cout << jeekText << std::endl;
}

void Jeek::addComment(std::string commentId)
{
	commentIds.push_back(commentId);
}

void Jeek::reJeek(std::string sessionId)
{
	JeekJeek::instance()->addJeek("Rejeeked:" + jeekText, tags, mentions, sessionId);
	reJeekCount++;
}

void Jeek::like(std::string userId)
{
	if (!hasLiked(userId))
	{
		likers.push_back(userId);
	}
}

void Jeek::dislike(std::string userId)
{
	if (hasLiked(userId))
	{
		likers.erase(find (likers.begin(), likers.end(), userId));
	}
}

std::string Jeek::HTMLForm()
{
	std::string result = readFile("assets/htmlFiles/templates/jeek.html");

	std::string writerName = JeekJeek::instance()->data()->findUserById(writerId)->getName();
	const std::string WRITER = "$(writer)";
	result = findAndReplaceAll(result, WRITER, writerName + ":");
	
	const std::string TEXT = "$(text)";
	result = findAndReplaceAll(result, TEXT, jeekText);

	std::string allTags;
	for (auto tag : tags)
	{
		allTags += ("#" + tag + " ");
	}
	const std::string TAG = "$(tags)";
	result = findAndReplaceAll(result, TAG, allTags);
	
	const std::string ID = "$(JeekId)";
	result = findAndReplaceAll(result, ID, id);
	
	return result;	
}

std::string Jeek::getMoreDetails(std::string userId)
{
	std::string result = readFile("assets/htmlFiles/templates/details.html");

	std::string writerName = JeekJeek::instance()->data()->findUserById(writerId)->getName();
	const std::string WRITER = "$(writer)";
	result = findAndReplaceAll(result, WRITER, "@" + writerName + ":");

	const std::string TEXT = "$(text)";
	result = findAndReplaceAll(result, TEXT, jeekText);

	std::string allTags;
	for (auto tag : tags)
	{
		allTags += ("#" + tag + " ");
	}
	const std::string TAG = "$(tags)";
	result = findAndReplaceAll(result, TAG, allTags);

	std::string allMentions;
	for (auto userId : mentions)
	{
		allMentions += ("@" + userId + " ");
	}
	const std::string MENTIONS = "$(mentions)";
	result = findAndReplaceAll(result, MENTIONS, allMentions);

	const std::string ID = "$(JeekId)";
	result = findAndReplaceAll(result, ID, id);
	
	const std::string LIKES = "$(Likes)";
	result = findAndReplaceAll(result, LIKES, std::to_string(likers.size()));

	const std::string REJEEKS = "$(Rejeeks)";
	result = findAndReplaceAll(result, REJEEKS, std::to_string(reJeekCount));

	if(hasLiked(userId))
	{
		result = findAndReplaceAll(result, "fa-heart-o", "");
	}

	return result;	
}

bool Jeek::hasLiked(std::string userId)
{
	return (find (likers.begin(), likers.end(), userId) != likers.end());
}

std::string Jeek::LikeAndRejeekNum()
{
	std::string result = "#Likes : $(Likes) / #Rejeeks : $(Rejeeks)";

	const std::string LIKES = "$(Likes)";
	result = findAndReplaceAll(result, LIKES, std::to_string(likers.size()));

	const std::string REJEEKS = "$(Rejeeks)";
	result = findAndReplaceAll(result, REJEEKS, std::to_string(reJeekCount));

	return result;
}	