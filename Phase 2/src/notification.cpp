#include "notification.hpp"

Notification::Notification(std::string _username, std::string _id)
{
	username = _username;
	id = _id;
}

void LikeNotification::print()
{
	std::cout << username << " liked " << id << std::endl;
}

void DisLikeNotification::print()
{
	std::cout << username << " disliked " << id << std::endl;
}

void CommentNotification::print()
{
	std::cout << username << " commented on " << id << std::endl;
}

void ReplyNotification::print()
{
	std::cout << username << " replied " << id << std::endl;
}

void JeekNotification::print()
{
	std::cout << username << " jeeked " << id << std::endl;
}

void MentionNotification::print()
{
	std::cout << username << " mentioned you in " << id << std::endl;
}

void RejeekNotification::print()
{
	std::cout << username << " rejeeked " << id << std::endl;
}