#include "user.hpp"

#include <iostream>
#include <algorithm>

#include "jeekJeek.hpp"

User::User(std::string username, std::string _displayName,  std::string _password)
{
	id = username;
	password = _password;
	displayName = _displayName;
}

void User::addJeek(std::string jeekId)
{
	jeekIds.push_back(jeekId);
}

void User::printJeeks()
{
	for (auto id : jeekIds)
	{
		JeekJeek::instance()->data()->findJeekById(id)->searchPrint();
		std::cout << std::endl;
	}
}

void User::addFollower(std::string userId)
{
	std::vector<std::string>::iterator it;
	it = find (followers.begin(), followers.end(), userId);
	if (it == followers.end())
	{
		followers.push_back(userId);
	}
}

void User::removeFollower(std::string userId)
{
	std::vector<std::string>::iterator it;
	it = find (followers.begin(), followers.end(), userId);
	if (it != followers.end())
	{
		followers.erase(it);
	}
}

void User::addNotification(Notification* notification)
{
	notifications.push_back(notification);
}

void User::showNotifications()
{
	for (auto notification : notifications)
	{
		notification->print();
	}
	notifications.clear();
}

std::string User::notificationsNum()
{
	return std::to_string(notifications.size());	
}