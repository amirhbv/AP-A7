#include "reply.hpp"

#include <iostream>

#include "jeekJeek.hpp"

Reply::Reply(std::string _userId, std::string text)
{
	userId = _userId;
	replyText = text;
}

void Reply::addReply(std::string replyId)
{
	replies.push_back(replyId);
}

void Reply::print()
{
	std::cout << JeekJeek::instance()->data()->findUserById(userId)->getName() << std::endl;
	std::cout << replyText << std::endl;
	std::cout << "Replies:" << std::endl;
	for (auto replyId : replies)
		std::cout << replyId << std::endl;
}