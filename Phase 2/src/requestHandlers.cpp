#include "requestHandlers.hpp"

#include "jeekJeek.hpp"
#include "exceptions.hpp"
#include "../utils/utilities.hpp"
#include "utility.hpp"

#include <sstream>

Response* LoginHandler::callback(Request* req)
{
	Response *res = new Response;
	res->setHeader("Content-Type", "text/html");

	std::string username = req->getBodyParam("username");
	std::string password = req->getBodyParam("password");

	std::string body = "";
	std::string sessionId = "";
	try
	{
		JeekJeek::instance()->loginUser(username, password);
		body = "OK";
		sessionId = std::to_string(rand());
		JeekJeek::instance()->setSessionId(username, sessionId);
	}
	catch(Exception &e)
	{
		body = e.what();
	}

	res->setSessionId(sessionId);
	res->setBody(body);
	return res;
}

Response* SignupHandler::callback(Request* req)
{
	Response *res = new Response;
	res->setHeader("Content-Type", "text/html");

	std::string username = req->getBodyParam("username");
	std::string displayName = req->getBodyParam("displayName");
	std::string password = req->getBodyParam("password");
	
	std::string body;
	try
	{
		JeekJeek::instance()->addUser(username, displayName, password);
		body = "OK";
	}
	catch(Exception &e)
	{
		body = e.what();
	}

	res->setBody(body);
	return res;
}

Response* CheckDuplicatedHandler::callback(Request* req)
{
	Response *res = new Response;
	res->setHeader("Content-Type", "text/html");

	std::string username = req->getBodyParam("username");
	
	std::string body;
	if(JeekJeek::instance()->isUsernameDuplicated(username))
	{
		body = "Username is Already Taken!";
	}
	else
	{
		body = "";
	}

	res->setBody(body);
	return res;
}

Response* LogoutHandler::callback(Request* req)
{
	Response *res = new Response;
	res->setHeader("Content-Type", "text/html");

	std::string sessionId = req->getSessionId();
	JeekJeek::instance()->logOut(sessionId);
	res->setSessionId("");
	res->setBody("");

	return res;
}

Response* GetDisplayName::callback(Request* req)
{
	Response *res = new Response;
	res->setHeader("Content-Type", "text/html");

	std::string sessionId = req->getSessionId();
	std::string body = JeekJeek::instance()->getDisplayName(sessionId);
	res->setBody(body);

	return res;
}

Response* SearchHandler::callback(Request* req)
{
	Response *res = new Response;
	res->setHeader("Content-Type", "text/html");

	std::string sessionId = req->getSessionId();
	std::string query = req->getBodyParam("query");
	if(query == "@")
	{
		query += JeekJeek::instance()->getUserName(req->getSessionId());
	}
	std::string body = searchResults(JeekJeek::instance()->search(query));
	res->setBody(body);

	return res;
}

std::string SearchHandler::searchResults(std::vector<std::string> jeekIds)
{
	reverse(jeekIds.begin(), jeekIds.end());
	std::string res;
	for (auto jeekId : jeekIds)
	{
		res += JeekJeek::instance()->data()->findJeekById(jeekId)->HTMLForm();
	}
	return res;
}

Response* GetMoreDetails::callback(Request* req)
{
	Response *res = new Response;
	res->setHeader("Content-Type", "text/html");

	std::string body = "";
	try
	{
		std::string sessionId = req->getSessionId();
		std::string jeekId = req->getBodyParam("jeekId");
		body = JeekJeek::instance()->getMoreDetails(jeekId, sessionId);
	}
	catch(Exception &e)
	{
		body = e.what();
	}
	res->setBody(body);

	return res;
}

Response* LikeHandler::callback(Request* req)
{
	Response *res = new Response;
	res->setHeader("Content-Type", "text/html");

	try
	{
		std::string sessionId = req->getSessionId();
		std::string jeekId = req->getBodyParam("jeekId");
		std::cout << jeekId << std::endl;
		JeekJeek::instance()->toggleLike(jeekId, sessionId);
	}
	catch(Exception &e)
	{ }
	res->setBody("");

	return res;
}

Response* RejeekHandler::callback(Request* req)
{
	Response *res = new Response;
	res->setHeader("Content-Type", "text/html");

	std::string body = "";
	try
	{
		std::string sessionId = req->getSessionId();
		std::string jeekId = req->getBodyParam("jeekId");
		JeekJeek::instance()->reJeek(jeekId, sessionId);
		body = "OK";
	}
	catch(Exception &e)
	{ }
	res->setBody(body);

	return res;
}

Response* SyncHandler::callback(Request* req)
{
	Response *res = new Response;
	res->setHeader("Content-Type", "text/html");

	std::string body = "";
	try
	{
		std::string jeekId = req->getBodyParam("jeekId");
		body = JeekJeek::instance()->likeAndRejeekNum(jeekId);
	}
	catch(Exception &e)
	{ }
	res->setBody(body);

	return res;
}

Response* NotiSyncHandler::callback(Request* req)
{
	Response *res = new Response;
	res->setHeader("Content-Type", "text/html");

	std::string body = "";
	try
	{
		std::string sessionId = req->getSessionId();
		body = JeekJeek::instance()->notificationsNum(sessionId);
	}
	catch(Exception &e)
	{ }
	res->setBody(body);

	return res;
}

Response* NewJeekHandler::callback(Request* req)
{
	Response *res = new Response;
	res->setHeader("Content-Type", "text/html");

	std::string body = "";
	try
	{
		std::string sessionId = req->getSessionId();
		std::string text = req->getBodyParam("text");
		std::vector<std::string> tag = split(req->getBodyParam("tags"));
		std::vector<std::string> mention = split(req->getBodyParam("mentions"));
		JeekJeek::instance()->addJeek(text, tag, mention, sessionId);
		body = "OK";
	}
	catch(Exception &e)
	{ 
		body = e.what();
	}
	res->setBody(body);

	return res;
}