#include "utility.hpp"

#include <sstream>
#include <iostream>

std::string removePrecedingSpaces(const std::string& str)
{
	int i = 0;
	while (str[i] == ' ')
		i++;
	return str.substr(i);
}

int strToInt(std::string s)
{
	std::stringstream ss(s);
	int res;
	ss >> res;
	return res;
}

std::string findAndReplaceAll(std::string src, const std::string query, std::string replace)
{
	while(src.find(query) != std::string::npos)
	{
		src.replace(src.find(query), query.size(), replace);
	}
	return src;
}

std::vector<std::string> split(const std::string &s, char delim)
{
    std::vector<std::string> elems;
    std::stringstream inputStream(s);
    std::string temp;
    while(std::getline(inputStream, temp, delim))
    {
    	elems.push_back(temp);
    }
    return elems;
}
