#include "jeekJeek.hpp"

JeekJeek* JeekJeek::_instance = nullptr;

JeekJeek* JeekJeek::instance()
{
	if (JeekJeek::_instance == nullptr)
	{
		JeekJeek::_instance = new JeekJeek;
	}
	return JeekJeek::_instance;
}

JeekJeek::JeekJeek()
{
	Logger::instance()->log(std::string(__FILE__) + " : JeekJeek made");

	dataManager = new DataManager;
}

void JeekJeek::run()
{
	// load data
	// inputManager->run();
	// save data
}

bool JeekJeek::isUserActive()
{
	return !activeUserId.empty();
}

bool JeekJeek::isJeekTextOk(std::string jeekText)
{
	return !jeekText.empty() && jeekText.size() < MAXIMUM_JEEK_LENGTH;
}

bool JeekJeek::isUsernameDuplicated(std::string username)
{
	User* user = dataManager->findUserById(username);
	return (user != NOT_FOUND);
}

std::string JeekJeek::getDisplayName(std::string sessionId)
{
	User* user = dataManager->findUserById(sessionIds[sessionId]);
	if (user != NOT_FOUND)
	{
		return user->getName();
	}
	return "";
}

std::string JeekJeek::getUserName(std::string sessionId)
{
	return sessionIds[sessionId];
}

void JeekJeek::addUser(std::string username, std::string displayName,  std::string password)
{
	Logger::instance()->log(std::string(__FILE__) + " : adding new user, user id : " + username);

	User* user = dataManager->findUserById(username);
	if (user != NOT_FOUND)
	{
		throw DuplicateUsername();
	}
	if (username.empty() || displayName.empty() || password.empty())
	{
		throw InvalidInput();
	}
	dataManager->addUser(new User(username, displayName, password));

	Logger::instance()->log(std::string(__FILE__) + " : new user added, user id : " + username);
}


void JeekJeek::loginUser(std::string username, std::string password)
{
	Logger::instance()->log(std::string(__FILE__) + " : trying to login, user id : " + username);

	if (username.empty() || password.empty())
	{
		throw InvalidInput();
	}

	User* user = dataManager->findUserById(username);
	if (user != NOT_FOUND)
	{
		if (user->checkPassword(password))
		{
			activeUserId = user->getId();

			Logger::instance()->log(std::string(__FILE__) + " : user logged in, user id : " + username);
		}
		else
		{
			throw WrongPassword();		
		}
	}
	else
	{
		throw UserNotAvailable();
	}
}

void JeekJeek::setSessionId(std::string username, std::string sessionId)
{
	sessionIds[sessionId] = username;
}

void JeekJeek::logOut(std::string sessionId)
{
	Logger::instance()->log(std::string(__FILE__) + " : user logged out, user id : " + activeUserId);

	sessionIds[sessionId] = "";	

	activeUserId = "";
}

std::vector<std::string> JeekJeek::search(std::string query)
{
	char type = query[0];
	query = query.substr(1);
	if(type == '@')
	{
		return searchUser(query);
	}
	else if(type == '#')
	{
		return searchTag(query);
	}
	else
	{
		throw InvalidInput();
	}
}

std::vector<std::string> JeekJeek::searchUser(std::string query)
{
	Logger::instance()->log(std::string(__FILE__) + " : searching for user, id : " + query);

	User* user = dataManager->findUserById(query);
	if (user != NOT_FOUND)
	{
		return user->jeeks();
	}
	else
	{
		return std::vector<std::string>(0);
	}
}

std::vector<std::string> JeekJeek::searchTag(std::string query)
{
	Logger::instance()->log(std::string(__FILE__) + " : searching for tag, id : " + query);

	return dataManager->findTag(query);
}

std::string JeekJeek::getMoreDetails(std::string jeekId, std::string sessionId)
{
	activeUserId = getUserName(sessionId);

	Jeek* jeek = dataManager->findJeekById(jeekId);

	return jeek->getMoreDetails(activeUserId);
}

void JeekJeek::toggleLike(std::string jeekId, std::string sessionId)
{
	activeUserId = getUserName(sessionId);

	Jeek* jeek = dataManager->findJeekById(jeekId);
	if(jeek->hasLiked(activeUserId))
	{
		dislike(jeekId);
	}
	else
	{
		like(jeekId);
	}
}
void JeekJeek::like(std::string jeekId)
{
	if (!isUserActive())
	{
		throw LoginException();
	}
	Jeek* jeek = dataManager->findJeekById(jeekId);
	if (jeek != NOT_FOUND)
	{
		jeek->like(activeUserId);
		dataManager->findUserById(jeek->getUserId())->addNotification(new LikeNotification(activeUserId, jeek->getId()));
	}
	else
	{
		throw JeekNotAvailable();
	}

	Logger::instance()->log(std::string(__FILE__) + " : user id : " + activeUserId + " liked jeek id : " + jeekId);
}

void JeekJeek::dislike(std::string jeekId)
{
	if (!isUserActive())
	{
		throw LoginException();
	}
	Jeek* jeek = dataManager->findJeekById(jeekId);
	if (jeek != NOT_FOUND)
	{
		jeek->dislike(activeUserId);
		dataManager->findUserById(jeek->getUserId())->addNotification(new DisLikeNotification(activeUserId, jeek->getId()));
	}
	else
	{
		throw JeekNotAvailable();
	}

	Logger::instance()->log(std::string(__FILE__) + " : user id : " + activeUserId + " disliked jeek id : " + jeekId);
}

void JeekJeek::reJeek(std::string jeekId, std::string sessionId)
{
	activeUserId = getUserName(sessionId);

	Logger::instance()->log(std::string(__FILE__) + " : user is rejeeking, user id : " + activeUserId);

	if (!isUserActive())
	{
		throw LoginException();
	}
	Jeek* jeek = dataManager->findJeekById(jeekId);
	if (jeek != NOT_FOUND)
	{
		jeek->reJeek(sessionId);
		dataManager->findUserById(jeek->getUserId())->addNotification(new RejeekNotification(activeUserId, jeek->getId()));
	}
	else
	{
		throw JeekNotAvailable();
	}

	Logger::instance()->log(std::string(__FILE__) + " : jeek rejeeked, jeek id : " + jeekId);
}

std::string JeekJeek::likeAndRejeekNum(std::string jeekId)
{
	Jeek* jeek = dataManager->findJeekById(jeekId);

	return jeek->LikeAndRejeekNum();
}

std::string JeekJeek::notificationsNum(std::string sessionId)
{
	activeUserId = getUserName(sessionId);
	User* user = dataManager->findUserById(activeUserId);

	return user->notificationsNum();
}

void JeekJeek::addJeek(std::string jeekText, std::vector<std::string> tags, std::vector<std::string> mentions, std::string sessionId)
{
	activeUserId = getUserName(sessionId);

	Logger::instance()->log(std::string(__FILE__) + " : user is jeeking, user id : " + activeUserId);

	if (!isUserActive())
	{
		throw LoginException();
	}
	if (!isJeekTextOk(jeekText))
	{
		throw JeekException();
	}

	Jeek *newJeek = new Jeek(activeUserId, jeekText, tags, mentions);
	dataManager->addJeek(newJeek);
	dataManager->findUserById(activeUserId)->addJeek(newJeek->getId());
	for (auto tag : tags)
	{
		dataManager->addTag(tag, newJeek->getId());
	}
	for (auto userId : mentions)
	{
		User *user = dataManager->findUserById(userId);
		if(user != NOT_FOUND)
		{
			user->addNotification(new MentionNotification(activeUserId, newJeek->getId()));
		}
	}
	std::vector<std::string> followers = dataManager->findUserById(activeUserId)->getFollowers();
	for (auto userId : followers)
	{
		User *user = dataManager->findUserById(userId);
		if(user != NOT_FOUND)
		{
			user->addNotification(new JeekNotification(activeUserId, newJeek->getId()));
		}
	}

	Logger::instance()->log(std::string(__FILE__) + " : new jeek added, jeek id : " + newJeek->getId());
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////// 


void JeekJeek::addComment(std::string jeekId, std::string commentText)
{
	Logger::instance()->log(std::string(__FILE__) + " : user is commenting, user id : " + activeUserId);

	if (!isUserActive())
	{
		throw LoginException();
	}
	Jeek* jeek = dataManager->findJeekById(jeekId);
	if (jeek != NOT_FOUND)
	{
		Comment* newComment = new Comment(activeUserId, commentText);
		dataManager->addComment(newComment);
		jeek->addComment(newComment->getId());
		dataManager->findUserById(jeek->getUserId())->addNotification(new CommentNotification(activeUserId, jeek->getId()));

		Logger::instance()->log(std::string(__FILE__) + " : new comment added, comment id : " + newComment->getId());
	}
	else
	{
		throw JeekNotAvailable();
	}

}

void JeekJeek::addReply(std::string id, std::string replyText)
{
	Logger::instance()->log(std::string(__FILE__) + " : user is replying, user id : " + activeUserId);

	if (!isUserActive())
	{
		throw LoginException();
	}
	if (id[0] == 'C')
	{
		replyComment(id, replyText);
	}
	else if (id[0] == 'R')
	{
		replyReply(id, replyText);
	}
	else
	{
		throw IdNotAvailable();
	}
}

void JeekJeek::replyComment(std::string id, std::string replyText)
{
	Comment *comment = dataManager->findCommentById(id);
	if (comment != NOT_FOUND)
	{
		Reply* newReply = new Reply(activeUserId, replyText);
		dataManager->addReply(newReply);
		comment->addReply(newReply->getId());
		dataManager->findUserById(comment->getUserId())->addNotification(new ReplyNotification(activeUserId, comment->getId()));

		Logger::instance()->log(std::string(__FILE__) + " : new reply added, reply id : " + newReply->getId());
	}
	else
	{
		throw CommentNotAvailable();
	}

}

void JeekJeek::replyReply(std::string id, std::string replyText)
{
	Reply *reply = dataManager->findReplyById(id);
	if (reply != NOT_FOUND)
	{
		Reply* newReply = new Reply(activeUserId, replyText);
		dataManager->addReply(newReply);
		reply->addReply(newReply->getId());
		dataManager->findUserById(reply->getUserId())->addNotification(new ReplyNotification(activeUserId, reply->getId()));

		Logger::instance()->log(std::string(__FILE__) + " : new reply added, reply id : " + newReply->getId());
	}
	else
	{
		throw ReplyNotAvailable();
	}
}

void JeekJeek::showJeek(std::string jeekId)
{
	Jeek* jeek = dataManager->findJeekById(jeekId);
	if (jeek != NOT_FOUND)
	{
		jeek->print();
	}
	else
	{
		throw JeekNotAvailable();
	}

	Logger::instance()->log(std::string(__FILE__) + " : jeek show, jeek id : " + jeekId);
}

void JeekJeek::showComment(std::string commentId)
{
	Comment* comment = dataManager->findCommentById(commentId);
	if (comment != NOT_FOUND)
	{
		comment->print();
	}
	else
	{
		throw CommentNotAvailable();
	}

	Logger::instance()->log(std::string(__FILE__) + " : comment show, jeek id : " + commentId);
}

void JeekJeek::showReply(std::string replyId)
{
	Reply* reply = dataManager->findReplyById(replyId);
	if (reply != NOT_FOUND)
	{
		reply->print();
	}
	else
	{
		throw ReplyNotAvailable();
	}

	Logger::instance()->log(std::string(__FILE__) + " : reply show, jeek id : " + replyId);
}

void JeekJeek::follow(std::string userId)
{
	if (!isUserActive())
	{
		throw LoginException();
	}
	User* user = dataManager->findUserById(userId);
	if (user != NOT_FOUND)
	{
		user->addFollower(activeUserId);
	}
	else
	{
		throw UserNotAvailable();
	}

	Logger::instance()->log(std::string(__FILE__) + " : user id : " + activeUserId + " followed user id : " + userId);
}

void JeekJeek::unfollow(std::string userId)
{
	if (!isUserActive())
	{
		throw LoginException();
	}
	User* user = dataManager->findUserById(userId);
	if (user != NOT_FOUND)
	{
		user->removeFollower(activeUserId);
	}
	else
	{
		throw UserNotAvailable();
	}

	Logger::instance()->log(std::string(__FILE__) + " : user id : " + activeUserId + " unfollowed user id : " + userId);
}

void JeekJeek::showNotifications()
{
	if (!isUserActive())
	{
		throw LoginException();
	}
	User* user = dataManager->findUserById(activeUserId);
	user->showNotifications();

	Logger::instance()->log(std::string(__FILE__) + " : user id : " + activeUserId + " requested for notifications");
}