#include "jeekJeek.hpp"
#include "requestHandlers.hpp"
#include "exceptions.hpp"

#include "utility.hpp"

void init()
{
	JeekJeek::instance()->addUser("a", "a", "a");
	JeekJeek::instance()->loginUser("a", "a");

	JeekJeek::instance()->setSessionId("a", "a");
	std::vector<std::string> tag;
	tag.push_back("amir");
	std::vector<std::string> mention;
	std::string text = "hi";
	JeekJeek::instance()->addJeek(text, tag, mention, "a");
	JeekJeek::instance()->addJeek("salamdjfnlsadkfasldkjsljfnaslkjffjlkfjl;salamdjfnlsadkfasldkjsljfnaslkjffjlkfjl;aksfjalksfdnasjknskjlfdnanalkjfnaskljfnaskjfdnasjkfasbnfajf", tag, mention, "a");
	JeekJeek::instance()->addJeek("khubi ?", tag, mention, "a");


	JeekJeek::instance()->logOut("a");
}

int main(int argc, char **argv)
{
	
	init();
	Server server(argc > 1 ? atoi(argv[1]) : 5000,
		"assets/htmlFiles/404.html");
	try {
		server.get("/", new ShowPage("assets/htmlFiles/home.html"));
		server.get("/home", new ShowPage("assets/htmlFiles/home.html"));
		server.post("/checkDuplicatedUsername", new CheckDuplicatedHandler());
		server.post("/login", new LoginHandler());
		server.post("/signup", new SignupHandler());
		server.post("/logout", new LogoutHandler());
		server.post("/search", new SearchHandler());
		server.post("/getMoreDetails", new GetMoreDetails());
		server.post("/like", new LikeHandler());
		server.post("/rejeek", new RejeekHandler());
		server.post("/syncLikeAndRejeekNum", new SyncHandler());
		server.post("/syncNotificationsNum", new NotiSyncHandler());
		server.post("/newJeek", new NewJeekHandler());
		server.get("/getDisplayName", new GetDisplayName());
	}
	catch (...)
	{	}

	try {
		server.run();
	} catch (Server::Exception e)
	{
		std::cerr << e.getMessage() << std::endl;
	}
	catch (Exception &e)
	{
		std::cerr << e.what() << std::endl;
	}
	catch (...)
	{	}
	return 0;
}
