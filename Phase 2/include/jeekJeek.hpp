#ifndef __JEEK_JEEK_H__
#define __JEEK_JEEK_H__

#include <string>
#include <vector>

#include "user.hpp"
#include "jeek.hpp"
#include "reply.hpp"
#include "logger.hpp"
#include "comment.hpp"
#include "exceptions.hpp"
#include "dataManager.hpp"
#include "notification.hpp"

const int MAXIMUM_JEEK_LENGTH = 140;

typedef std::string username;
typedef std::string sessionId;

class JeekJeek
{
public:
	JeekJeek();

	static JeekJeek* instance();
	DataManager* data() { return dataManager; }

	void run();

	bool isUserActive();
	bool isJeekTextOk(std::string jeekText);
	bool isUsernameDuplicated(std::string username);
	void setSessionId(std::string username, std::string sessionId);
	std::string getDisplayName(std::string sessionId);
	std::string getUserName(std::string sessionId);

	void addUser(std::string username, std::string displayName,  std::string password);
	void loginUser(std::string username, std::string password);
	void logOut(std::string sessionId);
	std::vector<std::string> search(std::string query);
	std::vector<std::string> searchUser(std::string query);
	std::vector<std::string> searchTag(std::string query);
	std::string getMoreDetails(std::string jeekId, std::string sessionId);

	void reJeek(std::string jeekId, std::string sessionId);
	void addJeek(std::string jeekText, std::vector<std::string> tags, std::vector<std::string> mentions, std::string sessionId);

	void toggleLike(std::string jeekId, std::string sessionId);
	void like(std::string jeekId);
	void dislike(std::string jeekId);
	std::string likeAndRejeekNum(std::string jeekId);
	std::string notificationsNum(std::string sessionId);

	// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // 
	void addComment(std::string jeekId, std::string commentText);
	void addReply(std::string id, std::string replyText);
	void replyComment(std::string id, std::string replyText);
	void replyReply(std::string id, std::string replyText);
	void showJeek(std::string jeekId);
	void showComment(std::string commentId);
	void showReply(std::string replyId);
	void follow(std::string userId);
	void unfollow(std::string userId);
	void showNotifications();

private:
	static JeekJeek* _instance;

	DataManager *dataManager;
	std::string activeUserId;
	std::map<sessionId, username> sessionIds;

};

#endif