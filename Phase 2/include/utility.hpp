#ifndef __UTILITY_H__
#define __UTILITY_H__

#include <string>
#include <vector>

std::string removePrecedingSpaces(const std::string& str);
int strToInt(std::string s);
std::string findAndReplaceAll(std::string src, std::string find, std::string replace);
std::vector<std::string> split(const std::string &s, char delim = ' ');

#endif