#ifndef __REQUEST_HANDLERS_H__
#define __REQUEST_HANDLERS_H__

#include "../server/server.hpp"
#include <vector>

class LoginHandler : public RequestHandler
{
public:
	Response* callback(Request* req);
};

class SignupHandler : public RequestHandler
{
public:
	Response* callback(Request* req);
};

class CheckDuplicatedHandler : public RequestHandler
{
public:
	Response* callback(Request* req);
};

class LogoutHandler : public RequestHandler
{
public:
	Response* callback(Request* req);
};

class GetDisplayName : public RequestHandler
{
public:
	Response* callback(Request* req);
};

class SearchHandler : public RequestHandler
{
public:
	Response* callback(Request* req);
private:
	std::string searchResults(std::vector<std::string> jeekIds);
};

class GetMoreDetails : public RequestHandler
{
public:
	Response* callback(Request* req);
};

class LikeHandler : public RequestHandler
{
public:
	Response* callback(Request* req);
};

class RejeekHandler : public RequestHandler
{
public:
	Response* callback(Request* req);
};

class SyncHandler : public RequestHandler
{
public:
	Response* callback(Request* req);
};

class NotiSyncHandler : public RequestHandler
{
public:
	Response* callback(Request* req);
};

class NewJeekHandler : public RequestHandler
{
public:
	Response* callback(Request* req);
};

#endif