#ifndef __REPLY_H__
#define __REPLY_H__

#include <string>
#include <vector>

class Reply
{
public:
	Reply(std::string _userId, std::string text);
	std::string getId() {return id; };
	std::string getUserId() {return userId; };
	void setId(std::string _id) { id = _id; };
	void addReply(std::string replyId);
	void print();
private:
	std::string id;
	std::string userId;
	std::string replyText;
	std::vector<std::string> replies;
};

#endif