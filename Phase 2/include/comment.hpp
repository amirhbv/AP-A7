#ifndef __COMMENT_H__
#define __COMMENT_H__

#include <string>
#include <vector>

#include "reply.hpp"

class Comment
{
public:
	Comment(std::string _userId, std::string text);
	std::string getId() {return id; };
	std::string getUserId() {return userId; };
	void setId(std::string _id) { id = _id; };
	void addReply(std::string replyId);
	void print();
private:
	std::string id;
	std::string userId;
	std::string commentText;
	std::vector<std::string> replies;
};

#endif