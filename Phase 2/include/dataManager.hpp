#ifndef __DATA_MANAGER_H__
#define __DATA_MANAGER_H__

#include <map>
#include <vector>
#include <algorithm>

#include "user.hpp"
#include "jeek.hpp"
#include "reply.hpp"
#include "utility.hpp"
#include "comment.hpp"
#include "exceptions.hpp"

#define NOT_FOUND nullptr

typedef std::string username;
typedef std::string jeekId;
typedef std::string tag;

class DataManager
{
public:
	DataManager() {};

	User* findUserById(std::string id);
	Jeek* findJeekById(std::string id);
	Comment* findCommentById(std::string id);
	Reply* findReplyById(std::string id);

	void addUser(User* user);
	void addJeek(Jeek* jeek);
	void addComment(Comment* comment);
	void addReply(Reply* reply);
	void addTag(std::string tag, std::string jeekId);
	std::vector<std::string> findTag(std::string tag);
private:
	std::vector<User*> users;
	std::vector<Jeek*> jeeks;
	std::vector<Comment*> comments;
	std::vector<Reply*> replies;
	std::map<tag, std::vector<jeekId> > tagMap;

	std::string makeJeekId();
	std::string makeCommentId();
	std::string makeReplyId();


};

#endif